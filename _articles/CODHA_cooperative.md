---
layout: theme
title: "CODHA coopérative"
key: CODHA_cooperative
is_main: true
toc:
- "Contexte"
---

### Contexte

La CODHA (Coopérative de l'habitat associatif) est une coopérative à but non lucratif qui a été créée en 1994. Le but de la CODHA est de rassembler dans des coopératives des personnes qui souhaitent un autre type d'habitat ainsi qu'une autre qualité de vie. Les coopératives de la CODHA sont basées sur la participation, la convivialité et la solidarité des habitants.

Notre première activité présentera la CODHA ainsi que son fonctionnement. Nous nous pencherons notamment sur sa structure et la manière dont elle fonctionne.

Notre seconde activité présente l'outil que la CODHA a mis au point afin de faciliter la vie des habitants de leurs différentes coopératives. Nous nous pencherons sur son fonctionnement général, son partage à d'autres coopératives ainsi que ses futures améliorations.

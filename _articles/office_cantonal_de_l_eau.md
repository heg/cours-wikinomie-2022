---
layout: theme
title: "Office cantonal de l’eau"
key: office_cantonal_de_l_eau
is_main: true
toc:
- "Contexte"
- "Bibliographie"
---

### Contexte
Le Service de la planification de l’eau (SPDE) fait partie de l’Office cantonal de l’eau (OCEau). Ce service permet de gérer la politique des eaux du canton. Plusieurs domaines sont concernées :

- établir planifications intégrées pour les grands bassins versants (SPAGE) et  mise en œuvre des actions en découlant
- accompagner les projets de développements urbains et d’infrastructures
- gérer et mettre à jour les données « Eau » du SITG
- coordonner l’établissement de préavis dans le cadre des autorisations de construire
- superviser la réalisation des réseaux d’assainissement communaux et collectifs privés 
- fixer les conditions de raccordements des biens-fonds privés et contrôler leur conformité
- assurer la gestion opérationnelle et la planification financière du Fonds intercommunal d’assainissement (FIA)

Cela signifie que des données sont récoltées, analysées puis partagées. La plateforme principale pour distribuer des données est le SITG. Cela permet de mettre à disposition des cartes interactives faciles d’accès.

### Bibliographie
OCEau - Service de la planification de l'eau. [en ligne]. [Consulté le 11 mai 2021]. Disponible à l'adresse : https://www.ge.ch/organisation/oceau-service-planification-eau

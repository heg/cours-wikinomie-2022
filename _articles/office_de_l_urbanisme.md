---
layout: theme
title: "Office de l'urbanisme"
key: office_de_l_urbanisme
is_main: true
toc:
- "Contexte"
- "État des lieux"
- "Bibliographie"
---

### Contexte

<i>“L'office de l'urbanisme contribue à l'élaboration et à la mise en œuvre d'une politique rationnelle de l'utilisation du sol favorisant la construction de logements, le développement des activités économiques, sociales et culturelles. Il est chargé de maintenir un cadre de vie attractif et de mettre en valeur l'environnement naturel et bâti.”</i> <br>- Site internet de l’Office de l’urbanisme https://www.ge.ch/organisation/office-urbanisme (consulté le 27 mai 2021)

C'est dans ce contexte que nous analysons l'un des outils mis en place par l'office de l'urbanisme afin de centraliser le processus de concertation auprès des contribuables.

### État des lieux

L'office de l'urbanisme agit sur deux échelles :

**Ensemble du canton** : L'office de l'urbanisme agit à ce niveau en suivant <a href="https://www.ge.ch/document/plan-directeur-cantonal-2030-mis-jour">le plan directeur cantonal 2030.</a> Approuvé par la Confédération, celui-ci sert de guide aux actions de l'office afin de permettre l'évolution du territoire en respectant le développement des activités humaines et la gestion durable du territoire.

**PLQ - Plan Localisé de Quartier** : Ces plans d'affectation du sol visent à organiser les constructions et les espaces publics. L'office de l'urbanisme collabore à ce niveau avec les autorités communales, ainsi qu'avec les habitants du quartier par le biais d'un processus de consultation. Le développement de ces plans, ainsi que leur réalisation, prend entre 2 à 10 ans.

### Bibliographie
<ul>
<li>GE.CH, date inconnue. <i>Office de l'urbanisme </i>[en ligne]. [Consulté le 10/06/2021]. Disponible à l'adresse :<br>https://www.ge.ch/organisation/office-urbanisme</li>
<li>GE.CH, 28/01/2021. <i>Plan directeur cantonal 2030 mis à jour </i>[en ligne]. [Consulté le 10/06/2021]. Disponible à l'adresse :<br>https://www.ge.ch/document/plan-directeur-cantonal-2030-mis-jour</li>
<li>GE.CH, 07/12/2020. <i>Consulter les plans d'aménagement adoptés </i>[en ligne]. [Consulté le 10/06/2021]. Disponible à l'adresse :<br>https://www.ge.ch/consulter-plans-amenagement-adoptes/plans-localises-quartier</li>
</ul>

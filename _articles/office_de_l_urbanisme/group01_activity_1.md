---
layout: activity
key: office_de_l_urbanisme
title: Participer à un projet urbain
tags: [participation, service public, données ouvertes, aménagement du territoire]
toc:
- "Situation actuelle"
- "Analyse des données"
- "Recommandations"
- "Bibliographie"
---


### Situation actuelle

L'activité participer à un projet urbain a été mise en place par l'État de Genève dans le but de favoriser la concertation de tous les acteurs intéressés par l'aménagement du territoire et les différents projets en cours. 

Actuellement plusieurs démarches sont proposées :
- Séances d'information publique
- Visites de quartiers
- Ateliers
- Questionnaires ou enquêtes
- [Participer.ge.ch](https://participer.ge.ch/) (plateforme numérique)

Cette plateforme a été mise en place par le canton de Genève pour promouvoir le dialogue public et la participation citoyenne à l’échelle de la région en utilisant le numérique. Il s’agit d’une plateforme basée sur l’outil free open source “Decidim” qui a été développé par une communauté soutenue par la mairie de Barcelone. Cet outil numérique permet donc de poursuivre les démarches de concertation à distance. L’objectif était de concevoir un format pour élargir la portée des démarches en présentiel en proposant des modèles alternatifs afin de garantir une participation collective aussi vaste que possible. 

#### Règles de gouvernance

[Information et participation de la loi fédérale sur l'aménagement du territoire (LAT), article 4](https://www.fedlex.admin.ch/eli/cc/1979/1573_1573_1573/fr#a4)

[Information, article 134 participation et article 135 concertation de la Constitution de la République et canton de Genève, article 11](https://silgeneve.ch/legis/data/rsg_a2_00.htm)

[Élaboration du projet de plan localisé de quartier de la loi générale sur les zones de développement (LGZD), article 5A](https://silgeneve.ch/legis/data/rsg_L1_35.htm)

#### Responsables et autres acteurs

- SALMON Christophe – Directeur de la direction administrative et financière (Office de l'urbanisme) 

#### Résultats et utilisation

Malheureusement, le site n’a pas reçu le résultat escompté. Pour l’instant, il y a très peu de projets et de participations. L’une des raisons est que la plateforme est méconnue de la population, même de ceux qui s’intéressent à l’urbanisme et à l’évolution du territoire. D’une autre part, plusieurs projets qu’ils soient concertés ou non sur le site participer.ge.ch font l’objet de référendums. Le problème peut provenir, entre autres, d’un problème de communication et d’information. De plus, il est difficile pour le citoyen de se projeter sur des plans architecturaux ou des maquettes à l’échelle. Il est important de noter que les données ouvertes téléchargeables depuis le site ne sont plus à jour depuis janvier 2020. Dans le cadre de notre étude, nous nous sommes principalement intéressés aux projets d’améliorations en cours qui permettraient d’améliorer la participation citoyenne.

En effet, nous avons pris connaissance de plusieurs projets actuellement en étude qui permettraient aux citoyens de visionner en 3D les propositions se trouvant dans les plans localisés de quartier avec la possibilité de laisser des avis sur des questions précises, comme l’emplacement d’un bâtiment, d’un espace vert ou d’un carrefour, pour en citer quelques-uns. 

Ces prototypes étant confidentiels, il ne nous est malheureusement pas possible de donner une description plus précise, mais ces différents projets sont très avancés du point de vue de la wikinomie. Cela montre la volonté de l’État de Genève de faire participer ces citoyens à l’évolution de l’urbanisation sur son sol.

### Analyse des données

#### Source

- Plans Localisés de Quartier 
- SITG - Le système d'information du territoire à Genève
- Plan directeur cantonal

#### Type

Actuellement, les données des propositions ainsi que les interactions entre le public et des membres du service de concertation communication sont disponibles. 

En fonction de l'aboutissement des projets, des données géographiques et données concernant les bâtiments seront potentiellement aussi utilisées.

#### Raison

Cette plateforme a pour but de promouvoir le dialogue public et la participation citoyenne à l’échelle de la région en utilisant le numérique.

#### Règles et dispositions
La plateforme est actuellement ouverte à tous et tout le monde peut s'y inscrire avec une adresse mail valide. Il est nécessaire d'accepter les conditions d'utilisation du site afin de pouvoir participer activement. A l'interne, cette plateforme est gérée par le service de concertation communication du département du territoire.

La plateforme utilise le framework Decidim, ce qui implique qu’elle est libre et open source sous la licence AGPLv3 ou ultérieure.  

### Recommandations

Comme il l’a été noté précédemment, le site est très méconnu du public. Il est dès lors nécessaire de le promouvoir. Pour cela nous avons pensé à quelques recommandations qui permettraient de diversifier et d’élargir le degré de participation : 

- Des campagnes publicitaires pour informer la population sur l’existence de la plateforme numérique participer.ge.ch et son utilisation. Cela pourrait se faire par sa promotion sur différents réseaux sociaux ou lieux à haute visibilité. 
- La présentation de maquettes 3D lors des manifestations publiques telles que les foires ou des stands temporaires en ville. 

Les données téléchargeables devraient être mises à jour régulièrement, afin de correspondre aux licences auxquelles la plateforme souscrit.

Si les projets arrivent à terme, ils devraient être facilement accessibles à toutes et à tous via un site dédié ou mis en exergue sur le site existant. 

### Bibliographie

Participer à un projet urbain. ge.ch [en ligne]. Mise à jour le 7 décembre 2020. [Consulté le 24 mai 2021]. Disponible à l’adresse :<br>[https://www.ge.ch/participer-projet-urbain](https://www.ge.ch/participer-projet-urbain)

La plateforme publique de participation citoyenne pour Genève et sa région. participer [en ligne]. [Consulté le 24 mai 2021]. Disponible à l’adresse :<br>[https://participer.ge.ch/](https://participer.ge.ch/)

Plans localisés de quartier. ge.ch [en ligne]. Mise à jour le 7 décembre 2020. [Consulté le 26 mai 2021]. Disponible à l’adresse :<br>[https://www.ge.ch/consulter-plans-amenagement-adoptes/plans-localises-quartier](https://www.ge.ch/consulter-plans-amenagement-adoptes/plans-localises-quartier) 

SANTOS, Yoni, 2020. Les dé­marches par­ti­ci­pa­tives aux temps de la dis­tan­cia­tion so­ciale. Es­pa­zium [en ligne]. 17.06.2020. [Consulté le 6 juin 2021]. Disponible à l’adresse :<br>[https://www.espazium.ch/fr/actualites/les-demarches-participatives-aux-temps-de-la-distanciation-sociale](https://www.espazium.ch/fr/actualites/les-demarches-participatives-aux-temps-de-la-distanciation-sociale)

decidim, free open-source participatory democracy for cities and organisations. decidim.org [en ligne]. 2021. [Consulté le 05 juin 2021]. Disponible à l’adresse :<br>[https://decidim.org/](https://decidim.org/)

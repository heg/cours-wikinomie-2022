---
layout: activity
key: emploi
title: Inscription au chômage
tags: [Participation, intérêt public, emploi]
toc:
- "Situation actuelle"
- "Analyse des données"
- "Recommandations"
- "Bibliographie"
---

### Situation actuelle (service)
Le canton de Genève a le taux de chômage le plus élevé des cantons Suisse avec un taux de 5,2% en mai 2021, la situation du COVID-19 a modifié beaucoup de processus au sein de l'État de Genève, aujourd’hui les inscriptions au chômage se font via le site web “Inscription au chômage”. Le but de cela est de faciliter l’inscription mais aussi de l'accélérer, la population n’a plus besoin de se rendre au locaux en personne mais s’inscrivent aujourd’hui en ligne, cependant la possibilité d’amener les documents en personne est toujours possible pour les personnes n’ayant pas la capacité de s’inscrire en ligne.
 
#### Règles de gouvernance (service)
Le service fourni par la plateforme est de faciliter l’inscription au chômage qui peut parfois être fastidieuse pour certaines personnes. C’est après l’inscription notamment qu’elles peuvent avoir accès à la plateforme job-room.ch afin de leur permettre de faciliter leurs recherches d’emploi.
 
#### Responsables et autres acteurs (service)
- Office cantonal de l’emploi
- Office Régional de placement
- job-room.ch (SECO)

#### Résultats et utilisation (service)
Les informations pouvant être récoltées ne concernent pas les inscriptions au chômage via le service en ligne mais l’inscription dans sa globalité.
Le nombre d'inscriptions est en baisse depuis le mois de février, comme nous pouvons le constater sur l’image, provenant du site de l'État de Genève, le taux d’inscrits est stagnant à 5,2% pour le mois de mai 2021. 

![](https://2021.wikipolitics.ch/emploi/images/inscription_chomage_genève.PNG)

À titre de comparaison, le mois d’avril avait un taux de 5,3%. Nous pouvons observer cette évolution sur le graphique ci-dessous.
![](https://2021.wikipolitics.ch/emploi/images/Inscription_chomage_geneve_evolution.PNG)

### Analyse des données

Lors de l’inscription via le service en ligne, il est demandé si nous avons une disponibilité immédiate pour un entretien avec l’Office Régional de Placement (ORP), il est indispensable de l’être sinon l’inscription ne peut être faite. Enfin d’autres informations concernant votre capacité à suivre des vidéoconférences en ligne en demandant si l’accès à un ordinateur avec caméra et micro ainsi qu’une connexion internet est possible. D’autres informations notamment si la personne est bénéficiaire de prestations auprès de l’APG mais aussi si elle est frontalière.

Une fois ces étapes passée, le formulaire d’inscription demande différentes informations telles que:

- Nom, Prénom
- Date de naissance
- État civil
- Nationalité
- Copie recto-verso de la carte d’identité
- N° d’AVS
- Copie recto-verso de la carte AVS
- Pays de résidence
- Adresse de résidence
- Email
- N° de téléphone


En entrant ces données, vous acceptez les conditions générales qui sont disponibles ici, ces conditions permettent notamment de transmettre vos données à d’autres services de l’État pour des raisons juridiques mais aussi de les enregistrer et les garder sur leur serveur. 

Après avoir rempli toutes ces données, il est demandé d’établir votre situation actuelle et de choisir la caisse de chômage. Pour finir, l’inscription sur job-room est proposée en fin d’inscription pour faciliter la suite du processus.


#### Source

L’inscription en ligne pour le chômage se fait via ce lien : 

[Conditions générales du site de l'Etat de Genève ](https://ge.ch/edem-pub2/ocechomage/formulaire/Controler?action=login&documentId=ocechomage&mediaType=ji_html&ids=userData,draftData&portal=StandalonePortal )

Une fois dans ce lien, il est demandé de fournir toutes les informations nécessaires pour la complétion de l’inscription.

#### Type

Aucun open data n’est lié à ce service, et donc aucune échelle de qualité ne peut y être attribuée.
Cependant, les données d'entrée peuvent être utilisées afin d’élaborer des statistiques sur le chômage, notamment à Genève, cette tâche est simplifiée à l’aide de la numérisation de cela malgré le fait que les inscriptions physiques sont toujours d’actualités.

Les données qui sont transmises sont strictement personnelles et le consentement d’utilisation est fait automatiquement à la soumission du formulaire.

 
#### Raison

Les données fournies à l’Office cantonal de l’Emploi via le formulaire d’inscription vont lui permettre de créer le dossier propre à une personne. Ses informations sont également demandée lors d’une embauche au sein d’une entreprise, elles sont essentielles afin de combler tous les besoins en information pour : 
- Le versement des salaires
- Le suivi des postulations
- La mise en relation simplifiée avec des entreprises
- La mise à jour du statut de la personne nationalement


Ces données sont essentielles au bon déroulement du processus, elles sont notamment identiques aux divers documents fournis lors d’une inscription physique.


#### Règles et disposition

Les règles de gestion des données sont énoncées dans les conditions générales du site de l’État de Genève, concernant les statistiques d’utilisation du site, la plateforme logicielle Matomo est utilisée afin d’analyser les informations récoltées. Les données peuvent être partagées avec tous les services de l’État de Genève pour des raisons juridiques.

Les données en interne sont consultées par le conseiller attitré à la personne, elles ne peuvent pas être partagées avec une entreprise sans le consentement de la personne.

 
### Recommandations


Les recommandations que nous avons établies sont les suivantes: Ajouter des informations quant au nombre d'inscrits via la plateforme d’inscription en ligne, il serait notamment possible de fournir un OpenData sur les inscriptions au chômage afin de suivre son évolution journalièrement. Le service est d’ailleurs très peu documenté, il est nécessaire de donner plus d’information à la population telle que, le temps de traitement de son inscription, la demande d'acceptation de l’utilisation de ses données expressément à l’aide d’un bouton, la disponibilité d'une charte d’utilisation de la plateforme accessible à tous les utilisateurs, elle est pour l'instant indiquée dans les condition générales mais aucun lien d'accès n’est mis à disposition. C’est encore un nouveau service et il est normal d’avoir des lacunes mais il est préférable de régler au plus vite ces détails afin de satisfaire au mieux la doctrine du gouvernement ouvert.

### Bibliographie

État de Genève, 2021. _S'inscrire au chômage_. ge.ch [en ligne]. 10 février 2021. [Consulté le 08.06.2021]. Disponible à l'adresse : [https://www.ge.ch/inscrire-au-chomage](https://www.ge.ch/inscrire-au-chomage)

République et Canton de Genève, 2021. Les 21 domaines: 03. Travail et rémunération. Statistiques cantonales[en ligne]. mai 2021. [Consulté le 08.06.2021]. Disponible à l'adresse : [https://www.ge.ch/statistique/domaines/apercu.asp?dom=03_03](https://www.ge.ch/statistique/domaines/apercu.asp?dom=03_03 )

République et Canton de Genève, 2021. Les 21 domaines: 03. Travail et rémunération. Actualités[en ligne]. mai 2021. [Consulté le 11.06.2021]. Disponible à l'adresse : [https://www.ge.ch/statistique/actualites/welcome.asp?actu=4360&Actudomaine=03_03&mm1=06/01&aaaa1=2021&mm2=6/11&aaaa2=2021](https://www.ge.ch/statistique/actualites/welcome.asp?actu=4360&Actudomaine=03_03&mm1=06/01&aaaa1=2021&mm2=6/11&aaaa2=2021)

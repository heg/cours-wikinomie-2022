---
layout: activity
key: CODHA_cooperative
title: La CODHA
tags: [collaboration, logement]
toc:
- "Situation actuelle"
- "Analyse des données"
- "Recommandations"
- "Bibliographie"
---

### Situation actuelle

La situation actuelle du logement à Genève n'est pas bonne. En effet, il n'y pas assez de logements pour répondre à la demande. La chereté des loyers est également au coeur du problème.

La CODHA cherche à proposer un type de logement différent aux personnes qui le souhaitent. En effet, habiter en coopérative est très différent d'un logement traditionnel, comme nous l'explique Mr. Käser :

> Chaque coopérative est un peu comme une grande famille. Cela a ses avantages mais cela peut aussi parfois être pénible. Tout le monde se connaît, tout le monde est ami. Lorsque vous sortez de chez vous, il se peut que vous passiez plus d'une heure à sortir de l'immeuble car chaque personne croisée entame une conversation sur la vie de la coopérative.

Pour illustrer le principe de partage des ressources de la CODHA, nous pouvons citer le projet Codhality. Ce projet, semblable à Mobility, permet aux habitants des coopératives de la CODHA d'obtenir un véhicule afin de se déplacer en ville de Genève. Ces véhicules, au nombre de 6, ont été achetés par la CODHA. Le service est auto-financé par les habitants des coopératives via un système d'abonnement.

#### Règles de gouvernance

La CODHA est séparée en deux parties distinctes. Le comité est au sommet de la hiérarchie et c'est ce dernier qui définit la stratégie et la politique de la CODHA. Ce comité, composé de 15 personnes est élu par l'assemblée générale à laquelle chaque collaborateur peut participer. Le comité est séparé en 4 pôles et il se réunit 6 à 8 fois par année.

La deuxième partie de la CODHA est représentée par les coopératives. Les tailles des coopératives vont d'un seul immeuble à un petit éco-quartier qui comprend 85 logements. Chaque coopérative est auto-gérée par ses propres habitants. Cela leur permet de définir un cadre de vie qui répond aux attentes des habitants de cette coopérative.

#### Responsables et autres acteurs

- Comité de la CODHA
- Habitants des coopératives

#### Résultats et utilisation

Nous n'avons pas de résultats concrets à présenter mais les informations récoltées par le comité de la CODHA ont aidées à mieux gérer les nouveaux projets grâce à une expérience accrue du terrain. Nous n'avons pas non plus de chiffres précis sur l'utilisation mais sachant que la CODHA est beaucoup axée sur les liens sociaux, nous pouvons imaginer que le nombre d'idées / conseils qui circulent entre les habitants des coopératives et le comité est élevé.

### Analyse des données

#### Source

Les données sont le plus souvent récupérées via des réunions qui se déroulent dans la ou les coopératives concernées par le sujet. Il est également possible que des données soient récoltées via support informatique, notamment dans le contexte actuelle ou les réunions en présentiel sont réduites au strict minimum.

#### Type

La CODHA ne récupère par de données personnelles sur ses habitants. Ce qui intéresse les dirigeants de la CODHA, ce sont les expériences des habitants de ses différentes coopératives.

> Les futurs habitants d'une coopérative peuvent donner leur avis sur les plans d'architecte. La décision finale revient tout de même au comité mais nous prenons en compte, dans la mesure du possible, les envies et idées des futurs résidents.

#### Raison

Les données récupérées sont analysées dans le but d'aider à la prise de décision lors de projets futurs ou alors d'autres coopératives. En effet, les expériences partagées par les habitants permettent d'améliorer la vie des autres coopératives.

#### Règles et dispositon

Les données sont utilisées en interne afin d'aider à la prise de décision. Elles sont utilisées lorsque cela s'avère nécessaire mais il est difficile de définir une fréquence précise. Nous ne savons pas comment les données sont stockées mais il paraît évident qu'une base de données existe.

### Recommandations

Selon notre analyse, la CODHA est bien gérée. Nous n'avons pas de recommandations à ce niveau. Nous avons par contre discuté de l'avenir des coopératives à Genève et quelques points intéressants sont ressortis. Selon Mr. Käser, le gouvernement donne des signes encourageants pour le développement des coopératives. Malgré tout, cela ne va pas assez vite. Actuellement, 5% du parc immobilier genevois appartient à des coopératives. Une initiative circule actuellement afin que l'état s'engage à ce que 10% du parc appartienne à des coopératives dans 10 ans. Selon Mr. Käser, il est indispensable que cette initiative soit acceptée afin que les coopératives puissent s'implémenter de manière durable à Genève et que la qualité de vie des genevois prenne également l'ascenseur.


### Bibliographie

CODHA, 2021. Site de la CODHA [en ligne]. [Consulté le 8 juin 2021]. Disponible à l'adresse : [https://www.codha.ch/fr/accueil](https://www.codha.ch/fr/accueil)

Certaines informations proviennent de l'interview réalisée avec Mr. Käser, directeur de la maîtrise d'ouvrage à la CODHA.

---
layout: activity
key: office_des_autorisations_de_construire
title: APA numérique
tags: [autorisations de construction, service public, participation]
toc:
- "Situation actuelle"
- "Analyse des données"
- "Recommandations"
- "Bibliographie"
---

### Situation actuelle
Dans l'objectif de numérisation des services offerts par l'administration de l'État de Genève, l'Office des autorisations de construire mets à disposition depuis le 29 mai 2018, via la plateforme e-démarches, un page internet permettant de déposer une demande d'autorisation de construire par procédure accélérée (APA). Par la suite, elle a étendu ce service aux demandes d'autorisation de démolition par procédure accélérée (MPA).
En effet, une majorité des demandes de construire concernent des demandes pouvant être traitées de manière accélérée, tels que les transformations (modification intérieure d'un bâtiment), des constructions de faible importance ou provisoires ou pour des travaux de reconstruction présentant un caractère d'urgence.
Quant à la demande de démolition par procédure accélérée (MPA) s'applique à la démolition d'objets dont l'autorisation de construire a été, ou aurait pu être, soumise à la procédure accélérée.

#### Règles de gouvernance
Pour déposer une APA il faut avoir un compte e-démarches qui est disponible pour tous les citoyens de l’État de Genève. Une fois inscrit aux e-démarches, il suffit d’activer la rubrique « autorisations de construire » dans l’onglet « Accéder aux prestations ». Cela prend environ 1h.

#### Responsables et autres acteurs
L’office des autorisations de construire et tout office concerné par les autorisations de construire, soit, l’office de l’urbanisme, l’office cantonal du logement et de la planification foncière, l’office du patrimoine et des sites, l’office du registre foncier, l’office cantonal de l’énergie, l’office cantonal de l’environnement, l’office cantonal de l’eau et l’office cantonal de l’agriculture et de la nature.

#### Résultats et utilisation
L’État de Genève développant constamment les services de cet outil, cette plate-forme est de plus en plus connue et utilisée, comme l’ensemble des prestations e-démarches. De plus, l’État de Genève continue à la faire évoluer dans le sens des utilisateurs et demande donc régulièrement leurs avis à ceux-ci par le biais de questionnaires par exemple. Le but étant de l’adapter au mieux aux besoins des citoyens genevois.

### Analyse des données

#### Source
Les données collectées proviennent majoritairement de la saisie de l’utilisateur ainsi que des documents PDF qu’il peut/doit téléverser. Certains champs (parcelle, adresse,…) proviennent d’une sélection issue d’une carte ou annuaire (SITG).
Les données afférentes aux décisions proviennent des différents services devant statuer sur le dossier et en fonction de sa nature p. ex. Office cantonal de l'agriculture et de la nature, Office cantonal de l’eau,…

#### Type
Le type d’informations collectées est relatif à l’objet pour laquelle une demande de construction ou de démolition est demandée, soient des plans, des extraits cadastraux, etc. Il contient également des données afférentes au requérant, au mandataire ainsi que du propriétaire.

#### Raison
D’une manière générale, le site sert de suivi pour les demandes d’autorisations. Les informations collectées sont ensuite analysées par l’office des autorisations de construire afin de statuer sur la délivrance ou le rejet de la demande d’autorisation. Etant donné qu’une demande doit être traitée par différents services, cela permet également de suivre les évolutions et décisions à chacune des étapes. Les autorisations sont ensuite communiquées directement sur le site, qui permet au demandeur d’obtenir le document d’autorisation de construire.

#### Règles et disposition
Les données sont accessibles pour l’utilisateur ainsi que pour les collaborateurs de l’office des autorisations de construire.
Les autorisations de construire délivrées sont publiées dans la feuille d’avis officielle et peuvent faire l’objet d’un recours par des tiers. Par conséquent, un partie de ces informations (synoptique, dates des décisions) est disponible en libre consultation via SAD-Cons.
Etant donné qu’il s’agit d’un outil de travail de l’office, l’utilisation est quotidienne et régulière.

### Recommandations
Ce type de service apporte de la transparence et facilite les suivis des demandes de constructions mais elle est surtout dédiée aux professionnels du métier. Elle pourrait être complétée par des données ouvertes disponibles à tous pour une meilleure compréhension et suivi à la fois du service, mais aussi de la politique de la ville sur l’urbanisme.
D’une manière générale, on peut dire que les démarches sont bien documentées mais il est préférable que ce soit un professionnel du métier qui soit sollicité pour effectuer cette demande.

### Bibliographie
GE.CH, Office des autorisations de construire [en ligne]. [Consulté le 13 juin 2021] Disponible à l’adresse : [https://www.ge.ch/organisation/office-autorisations-construire](https://www.ge.ch/organisation/office-autorisations-construire)

GE.CH, 2 février 2021 Demander une APA numérique [en ligne]. [Consulté le 13 juin 2021] Disponible à l’adresse : [https://www.ge.ch/demander-apa-numerique](https://www.ge.ch/demander-apa-numerique)

GE.CH, 6 octobre 2020 Consulter une autorisation de construire [en ligne]. [Consulté le 13 juin 2021] Disponible à l’adresse : [https://www.ge.ch/consulter-autorisation-construire](https://www.ge.ch/consulter-autorisation-construire) 

Office des autorisations de construire, 2018. Demander une APA numérique - Vidéo [enregistrement vidéo]. ge.ch [en ligne]. 29 mai 2018. [Consulté le 13 juin 2021]. Disponible à l'adresse : [https://www.ge.ch/document/demander-apa-numerique-video](https://www.ge.ch/document/demander-apa-numerique-video)

GE.CH, SAD-Cons [en ligne]. [Consulté le 13 juin 2021] Disponible à l’adresse : [http://etat.geneve.ch/sadconsult/sadconsult.asp?WCI=frmConnectionHandler](http://etat.geneve.ch/sadconsult/sadconsult.asp?WCI=frmConnectionHandler)

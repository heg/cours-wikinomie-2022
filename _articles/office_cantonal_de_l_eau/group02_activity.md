---
layout: activity
key: office_cantonal_de_l_eau
title: SPAGE
tags: [information, collaboration, service public, environnement]
toc:
- "Situation actuelle"
- "Analyse des données"
- "Recommandations"
- "Bibliographie"
---

### Situation actuelle
Le service de la planification de l’eau possède un outil qui permet de gérer la planification politique publique de l’eau. Il s’agit du schéma de protection, d'aménagement et de gestion des eaux (SPAGE). Cet outil concerne les grands bassins versants hydrologiques du canton.

Il existe 6 SPAGE pour tout le canton qui sont les différents bassins versants hydrologiques:
- Aire - Drize
- Lac rive gauche
- Lac rive droite
- Lac - Rhône - Arve
- Allondon - Mandement
- Champagne - La Laire

Certaines thématiques vont mettre en évidence les problématiques liés au cours d’eau qui est au coeur du SPAGE. Il y a par exemple l’impact de l’agriculture, de la zone urbaine, des crues, de l'espace régionnal, des zones natures, etc.

#### Règles de gouvernance
Les données récoltées pour établir le SPAGE sont publiques. En effet, on en retrouve la plupart directement dans le SITG. Cependant, l'utilité et l'utilisation du SPAGE se font en interne. C'est un outil utilisé par les communes et le canton.

Le SPAGE est présent dans la loi cantonale sur les eaux (L 2 05, art. 13) et son règlement d'exécution (L 2 05.01, art. 7). Cela permet d'avoir 


#### Responsables et autres acteurs
Les différentes acteurs sont :
- Les services de l’Etat
- Les communes
- Les associations
- Le Conseil d’Etat
- Le Conseil du développement durable
- Les partenaires

Le responsable est l’Office cantonal de l’eau (OCEau). Cependant, le Conseil d’Etat doit approuver chaque SPAGE qui lui-même a été préavisé par le Conseil du développement durable.

Il y a aussi un contact fait avec des acteurs français car des cours d’eau sont transfrontaliers.

#### Résultats et utilisation
Actuellement, seuls 5 SPAGE sont élaborés et adaptés par le Conseil d’Etat. Le SPAGE “Champagne - La Laire” est en cours d’élaboration. Beaucoup de données sont générés grâce à ce document. Surtout de la cartographie.

### Analyse des données
#### Source
Les données sont récoltées grâce à la collaboration des acteurs cités précédemment. L’OCEau, les communes ainsi que les partenaires établissent les objectifs à atteindre avec des schémas. Cela prend un an pour élaborer un SPAGE. Ensuite, tous les 6 ans, il est revu et mis à jour. On vérifie si les objectifs ont été atteints ou non.

#### Type
Le SPAGE qui possèdent plusieurs informations:
La description du bassin
L'état des cours d’eau
Qualité de l’eau 
Régime hydrologique
Morphologie des cours d’eau et ouvrages
Espace nécessaire aux cours d’eau et Espace minimal
Valeurs naturelles et paysagères
L’entretien des cours d’eau
Les loisirs
Les eaux souterraines

#### Raison
Le SPAGE permet de coordonner les actions relatives à la gestion des bassins versants hydrographiques du canton. Il sert à planifier et à élaborer des actions. Le but du SPAGE est d’atteindre divers objectifs stratégiques. Cet outil prend la forme d’un rapport qui possède des cartographies.

#### Règles et dispositon
Une fois le SPAGE établie, il est publiquement disponible sur le site de l’Etat de Genève. On retrouve la liste des différents SPAGE sur ce lien : 
https://www.ge.ch/outils-planification-eau/schema-protection-amenagement-gestion-eaux-spage.

La plupart des données récoltées sont aussi mises en ligne sur le SITG. Elles sont accessibles depuis ce lien : 
https://www.etat.ge.ch/geoportail/pro/?mapresources=ASSAINISSEMENT%2CHYDROBIOLOGIE%2CHYDROGRAPHIE&hidden=ASSAINISSEMENT%2CHYDROBIOLOGIE

### Recommandations
Il est très important d'avoir une révision de l'outil regulièrement. Les données et les attentes peuvent évoluer au cours du temps. Malgré cela, le document qui explique ce que contient le SPAGE a été mis à jour pour la dernière fois le 9 janvier 2012.

Le cycle de 6 ans qui permet de vérifier si les objectifs du SPAGE ont été atteint est certainement trop long. Aujourd'hui tout va très vite, et les données changent aussi à grande vitesse. En 6 ans, des nouvelles technologies émergent et les attentes sont différentes. Ce n'est plus au goût du jour d'avoir des cycles aussi longs.

### Bibliographie
Office cantonal de l’eau (OCEau). [en ligne]. [Consulté le 11 mai 2021]. Disponible à l’adresse : https://www.ge.ch/organisation/office-cantonal-eau-oceau

Service de la planification de l'eau. [en ligne]. [Consulté le 11 mai 2021]. Disponible à l’adresse : https://www.ge.ch/organisation/oceau-service-planification-eau

Outils de planification de l'eau. [en ligne]. [Consulté le 11 mai 2021]. Disponible à l’adresse : https://www.ge.ch/outils-planification-eau

Schéma de protection, d'aménagement et de gestion des eaux (SPAGE). [en ligne]. [Consulté le 11 mai 2021]. Disponible à l’adresse : https://www.ge.ch/outils-planification-eau/schema-protection-amenagement-gestion-eaux-spage

SPAGE Outil cantonal de gestion intégrée des eaux par bassin versant. [fichier PDF]. [Consulté le 11 mai 2021]. Disponible à l’adresse : https://www.ge.ch/document/eau-spage-outil-cantonal-gestion-integree-eaux-bassin-versant

Loi sur les eaux. [en ligne]. [Consulté le 11 juin 2021]. Disponible à l'adresse : https://silgeneve.ch/legis/data/rsg_L2_05.htm

Règlement d’exécution de la loi sur les eaux. [en ligne]. [Consulté le 11 juin 2021]. Disponible à l'adresse : https://silgeneve.ch/legis/data/rsg_L2_05p01.htm

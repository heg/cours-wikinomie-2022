---
layout: activity
key: Suisse_COVID_app
title: Swiss Covid app
tags: [santé, sensibilisation, statistiques]
toc:
- "Situation actuelle"
- "Analyse des données"
- "Recommandations"
- "Bibliographie"
---
<img src="https://www.swisscyberforum.com/wp-content/uploads/2020/06/SwissCovid-1024x1024.jpeg" style="width: 200px;">
<p style="color: grey;"><i> Logo de l'application SwissCovid</i> </p>

### Situation actuelle
<div align="justify">
<p>Il s’agit d’une application mobile gratuite (Android et IOS) qui permet d’identifier les contacts avec les personnes infectés afin d’interrompre les chaînes de transmissions. Pour ce faire, elle utilise la technologie Bluetooth pour enregistrer les rencontres entre utilisateurs (1.5 mètres de distance et au moins 15 minutes). Lorsque qu’une personne et testée positive, il lui suffit d’entrer un code dans l’application qui notifiera toutes les personnes avec qui elle aura été en contact.</p>

<p>Le système sera désactivé quand la crise sera terminée.</p>
</div>
 
#### Règles de gouvernance
<div align="justify">
<p>Quelques statiques sont disponibles pour la population afin d’avoir un suivi de l’utilisation de l’application et de son efficacité. Cependant, aucune donnée personnelle n’est transmise afin de respecter la protection de la vie privée.</p>
</div>
<div style="display: flex;align-items: center; justify-content: space-around;">
  <img src="https://framagit.org/heg/cours-wikinomie-2021/-/wikis/uploads/9cad46cbe390a799f703ae1bf48a6edc/swissCovidAppScreen1.png" style="width: 30%;">
    <img src="https://framagit.org/heg/cours-wikinomie-2021/-/wikis/uploads/66cf4084c01d84595dd931a0160b89a8/swissCovidAppScreen2.png" style="width: 30%;">
</div>
<br/>
<div align="center">
<p style="color: grey;"><i>Screenshots de l'application SwissCovid</i> </p>
</div>


#### Responsables et autres acteurs
<table style="width:100%">
  <tr style="background-color: Gainsboro;">
    <th>Acteur</th>
    <th>Rôle</th> 
  </tr>
  <tr>
    <td>Office fédérale de la santé publique (OFSP)</td>
    <td> - Gestion des statistiques Covid-19 pour toute la Suisse</td> 
  </tr>
  <tr>
    <td>EPFL, ETHZ et Ubique (société) </td>
    <td>-	Développement de l’application SwissCovid app</td> 
  </tr>
</table>
 
#### Résultats et utilisation
<div align="justify">
<p>Dans l'onglet "statistiques" de l'application, il est reporté le nombre d’utilisateurs actifs, le nombre de cas ainsi que le nombre de code covid saisis.</p>
</div>
  
### Analyse des données 
#### Source
<div align="justify">
<p>Les données, qui sont anonymisées, proviennent des téléphones des utilisateurs.</p>
<p>L’office fédéral de l’informatique et de la technologie (OFIT) transmets chaque jour à l’OSPF (office fédéral de la statistique), le nombre d’application actives, le nombre de codes entrées et le nombre d’appels à l’info line.</p>
</div>
 
#### Type
<div align="justify">
<p>Des identifiants aléatoires sont attribués lors de l’installation de l’application. Lors de contact avec une personne, les codes d’identification vont être échangées. Les données vont être stockées pendant 14 jours avant d’être effacées automatiquement. Toutes ses données sont stockées uniquement localement.</p>
<p>Aucune donnée personnelle ou de géolocalisation ne sont stockée sur des serveurs ou autres emplacements externes. Il est donc impossible de retracer les déplacements d’une personne et de savoir ou elle était et avec qui.</p>
</div>

#### Raison
<div align="justify">
<p> Le but de l’application est d’interrompre les chaines de transmission du Covid-19.</p>
</div>
 
#### Règles et disposition
<div align="justify">
<p>L’application SwissCovid est open source et disponible sur GitHub (Source 3).</p>
</div>
 
### Recommandations
<div align="justify">
<p>Nous avons demandé à une personne de l’OSFP sa satisfaction sur l’application Swiss Covid App (Source 5).</p>

<p>Tout d’abord, elle est contente qu’une application pour la population ai pu être développée dans un délai aussi court. Ensuite, l’application permet de briser les chaines de d’infection. En effet, il a beaucoup de personnes qui sont allées faire un test après avoir reçu une notification de l’application SwissCovid et qui ont obtenus un résultat positif.</p>

<p>L’application est un soutien pour la recherche traditionnelles des contacts dans le canton. Elle permet notamment de gagner du temps. Il y a une étude du professeur Viktor Von Wyl de l'Université de Zurich qui résume bien les avantages de l'application SwissCovid d'un point de vue scientifique (Source 4).</p>

<p>L’OSPF est content du nombre d’utilisateurs de l’application malgré un nombre qui stagne entre 1.8-2 millions, mais aimerait encore augmenter le nombre d’utilisateurs. Ils examinent actuellement les possibilités pour promouvoir à nouveau l’application ainsi que son rôle dans les étapes d’ouverture.
</p>
</div>

### Bibliographie
<ol>
  <li>OFSP, Office fédéral de la santé publique. « Coronavirus : application SwissCovid et traçage des contacts ». Consulté le 10 juin 2021. <a href="https://www.bag.admin.ch/bag/fr/home/krankheiten/ausbrueche-epidemien-pandemien/aktuelle-ausbrueche-epidemien/novel-cov/swisscovid-app-und-contact-tracing.html">https://www.bag.admin.ch/bag/fr/home/krankheiten/ausbrueche-epidemien-pandemien/aktuelle-ausbrueche-epidemien/novel-cov/swisscovid-app-und-contact-tracing.html</a></li>
  <li>expérimentales, Office fédéral de la statistique (OFS)-Statistiques. « Suivi de l’application SwissCovid ». Consulté le 10 juin 2021. <a href="https://www.experimental.bfs.admin.ch/expstat/fr/home/innovative-methoden/swisscovid-app-monitoring.html">https://www.experimental.bfs.admin.ch/expstat/fr/home/innovative-methoden/swisscovid-app-monitoring.html</a></li>
  <li>SwissCovid/swisscovid-app-android. Java. 2020. Reprint, SwissCovid, 2021. <a href ="https://github.com/SwissCovid/swisscovid-app-android">https://github.com/SwissCovid/swisscovid-app-android</a></li>
  <li>« Swiss-Covid-App zeigt Wirkung ». Universität Zürich. Consulté le 10 juin 2021. <a href="https://www.news.uzh.ch/de/articles/2021/Swisscovidapp.html">https://www.news.uzh.ch/de/articles/2021/Swisscovidapp.html</a></li>
  <li><a href="https://framagit.org/heg/cours-wikinomie-2021/-/wikis/uploads/0fa7388bffee695583591b33ede4f7f1/InterviewGregoireGogniatOFSP.pdf">Interview Grégoire Gogniat, porte-parole OFSP, réalisé le 9 mai 2021</a></li>
</ol>


---
layout: activity
key: office_de_l_énergie
title: SIG-éco21
tags: [collaboration, service public, environnement]
toc:
- "Situation actuelle"
- "Analyse des données"
- "Recommandations"
- "Bibliographie"
---

### Situation actuelle
<div align="justify">
<img src="https://www.veyrier.ch/sites/default/files/images/eco21.png" style="margin-left: 15px; border: 0;" align="right" width="190" height="190" />
<p>GEnergie ne s’arrête pas qu’au service de subventions. En effet, les services industriels de l’Etat de Genève (SIG) disposent également d’un programme d’efficacité énergétique, en vigueur depuis 2007 et qui est disponible à travers GEnergie.</p>
<p>Ce programme, nommé éco21, vise les collectivités publiques, les PME, les particuliers et les régies immobilières et s’articule autour de trois actions : l’accompagnement dans la réalisation de plans, la mise en œuvre et l’octroi de subventions. Le projet vise à accompagner ces acteurs, dans le but de réduire leur consommation électrique et leurs émissions de CO<sub>2</sub>, en leur proposant un plan de modification prenant en compte les enjeux écologiques et les infrastructures du bénéficiaire. Ils mettent ensuite à disposition une liste d’entreprises partenaires, certifiées et donc capables de réaliser les travaux. Enfin, le SIG rachète l’économie projetée grâce aux travaux.</p>
</div>

#### Règles de gouvernance
<div align="justify">
<p>Étant introduit dans le projet GEnergie, les données liées au programme éco21 sont également disponibles via le formulaire, mais des demandes sont également adressées directement auprès du service en charge du projet.</p>
<p>Pour le reste, et à l’instar de GEnergie, les données récoltées dans le cadre du projet ne sont pas rendues publiques selon la LIPAD. Quant aux données issues des audits sur les infrastructures, elles ne le sont pas non plus et ne sont utilisées qu’en interne. En ce qui concerne les travaux, les bénéficiaires sont libres de transmettre les informations les concernant à des fins d’autopromotion.</p>
<p>Enfin, les données liées aux consommations des clients sont également uniquement réservées à l’interne. En effet, le SIG disposant d’un service commercial de vente d’énergie, il est immoral de rendre accessibles les données des clients.</p>
</div>

#### Responsables et autres acteurs
<div align="justify">
<ul>
  <li>Services industriels de Genève (SIG)</li>
  <li>Office cantonal de l’énergie (OCEN)</li>
  <li>Les entreprises mandatées (auditeurs inclus) dans le cadre du projet</li>
  <li>Les citoyens participants</li>
</ul>
</div>

#### Résultats et utilisation
<div align="justify">
<p>L’enveloppe dans laquelle éco21 pioche étant la même que celle mentionnée pour GEnergie, ils ont également dépensé l’entièreté du budget pour 2020. Même si les données liées au projet ne sont pas publiques, certaines sont publiées sont publiées sous forme de case-studies, dont celui de Honda Suisse est un exemple. En effet, grâce à l’optimisation de l’éclairage dans leur bureau, ils ont su réduire leur consommation d'énergie de 65%.</p>
</div>

### Analyse des données

#### Source
<div align="justify">
<ul>
  <li>Formulaire de demande de subventions</li>
  <li>Relevés de consommations</li>
  <li>Contact direct</li>
</ul>
</div>

#### Type
<div align="justify">
<ul>
  <li>Par le formulaire : données personnelles, énergétiques et caractéristiques sur le bâtiment. </li>
  <li>Par les relevés : la consommation énergétique brute et la surface de référence énergétique sont saisies via une application (Indice) puis stockées dans une base de données.</li>
  <li>Informations sur les infrastructures</li>
</ul>
</div>

#### Raison
<div align="justify">
<ul>
  <li>Pour le formulaire : mise en relation des citoyens participants avec l’Office.</li>
  <li>Pour les relevés : afin de monitorer l’évolution de la situation et de dresser des statistiques.</li>
  <li>Dans le but d’établir un plan d’optimisation</li>
</ul>
</div>

#### Règles et disposition
<div align="justify">
<ul>
  <li>Pour le formulaire et le contact direct : dans un cadre restreint aux particuliers, car les données personnelles sont protégées par la LIPAD.</li>
  <li>Pour les données de consommation : elles peuvent être fournies à des fins d’étude. De fait, elles seront soit anonymisées, soit soumises à un accord de confidentialité. Le format des données dépend donc du cas traité, et ne peuvent donc pas être classifiées selon leur degré d’ouverture. Les données relatives à l’ensemble des opérations sont transmises à l’État et à la population sous forme de rapport.</li>
</ul>
</div>


### Recommandations
<div align="justify">
<p>Suite à nos recherches, nous avons constaté que la majeure partie des case-studies réalisées concernent des entreprises. Nous pensons qu’équilibrer leur nombre avec des économies réalisées par des particuliers permettrait de promouvoir davantage le projet auprès des citoyens.</p>
</div>

### Interview
<div align="justify">
<p>Lors de nos recherches, nous avons pu poser quelques questions à Monsieur <strong>Frederik CHAPPUIS</strong>, Responsable Plan d'Actions au Programme éco21, lors d’un échange en visio. À noter qu'il s'agit d'un résumé et non d'une retranscription stricte de l'entrevue et que Monsieur CHAPPUIS a accepté d'être cité par nom.</p>
</div>

**Au niveau du formulaire de demande de subvention, nous l'avons testé fictivement et aimerions savoir quelles données sont récupérées et ce que vous en faites-vous après coup. Les liez-vous avec la consommation du SIG ? Utilisez-les vous à des fins statistiques pour monitorer l’évolution post-subvention ?**
> _Les données de demandes sont conservées pour le dossier du client. Elles ne sont pas utilisées, ni rendues disponible en dehors de ce cadre. Les subventions octroyées sont liées à l’économie réalisée et le projet est monitoré post-travaux._

**Quel type de données récoltez-vous ? Mettez-vous à disposition du public un retour sur le projet sous une autre forme et si oui, laquelle ?**
> _Les données fournies par les demandeurs sont confidentielles autant au niveau des données personnelles couvertes par la LIPAD que le reste de données qui pourraient être utilisées à des fins commerciales par d’autres acteurs. Les SIG étant également un distributeur d’énergie, ces données sont réservées aux départements liés au traitement du dossier et ne seront en aucun cas rendues disponibles pour les pôles commerciaux des SIG.
> Elles sont mises à disposition sous forme de rapport, reprenant l’ensemble des données, notamment lors d’une séance de présentation des résultats annuels. Cette méthode, permet de fournir un retour au politique ainsi qu’au citoyen tout en protégeant les données personnelles des acteurs ayant participé au projet. Certains projets sont également disponibles sous forme d’étude de cas._

**Sont-elles échangées/utilisées d'une autre façon ?**
> _L’ensemble des données de consommations ou liées aux différents projets peuvent être mises à disposition sur demande, notamment pour des cas d’études ou d’autres projets. Les données seront soit anonymisées, soit un accord de confidentialité sera signé avant la mise à disposition. 
> Les données sont également auditées afin de s’assurer de leur véracité avant qu’elles ne soient fournies au politique et à la population. Cet audit est réalisé par en partie par l’université de Genève ainsi qu’un cabinet de conseil._

**Est-ce que votre projet s'inscrit dans un projet plus global (cf. relation GEnergie) d'analyse/agrégat des données ? Que cela pourrait-il signifier sur la cartographie des données des consommateurs ?**
> _Les prestations font partie du catalogue proposé par GEnergie. Les données sont utilisées pour divers projets d’études. La mise en relation des données des consommateurs avec d’autres données est possible. Par exemple, l’efficacité énergétique ou encore la consommation de gaz pour le chauffage sont disponibles sur le site des SITG mais pas toutes._

**Que l’état compte-t-il faire suite aux résultats du projet ? Peut-on envisager une augmentation de l’enveloppe allouée à ce genre de politiques ?**
> _Le budget est dépendant de la même enveloppe que celle de GEnergie. Pour l’instant, la totalité de l’enveloppe budgétaire est dépensée chaque année. Le service, se charge également d’aller à la rencontre des gros consommateurs du canton pour leur proposer diverses prestations._

### Bibliographie
<div align="justify">
<ul>
  <li>SIG. <i>The SIG-éco21 programme.</i> [En ligne]. [Consulté le 20 mai 2021]. Disponible à l'adresse : <a href="https://ww2.sig-ge.ch/en/a-propos-de-sig/nous-connaitre/le-programme-eco21">https://ww2.sig-ge.ch/en/a-propos-de-sig/nous-connaitre/le-programme-eco21</a></li>
  <li>Eco21. <i>Outils Eco21.</i> [En ligne]. [Consulté le 20 mai 2021]. Disponible à l'adresse : <a href="https://outils.eco21.ch/">https://outils.eco21.ch/</a></li>
  <li>SIG. <i>Optiwatt - Comment optimiser la consommation de l'éclairage dans les bureaux. </i>[En ligne]. [Consulté le 20 mai 2021]. Disponible à l'adresse : <a href="https://ww2.sig-ge.ch/sites/default/files/inline-files/eclairage-honda_suisse.pdf">https://ww2.sig-ge.ch/sites/default/files/inline-files/eclairage-honda_suisse.pdf</a></li>
</ul>
</div>
